"""
Ising system quench and domain growth.
This file calculates all the results in the work.

Alpi Tolvanen, Licence CC0 i.e. public domain.
"""

import numpy as np
import matplotlib.pyplot as plt
import time

try:
    from numba import jit, prange
except ImportError:
    print("You do not have numba-library installed. Numba optimization makes python") 
    print("to run with c-level efficiency (50-100× faster), and threading (8× faster).")
    print("This script takes 2 hours with numba, so it will take eternity with without.")
    print("If you do not want to install numba, please")
    print("    - comment out this whole try-except -block")
    print("    - comment out all function declarators i.e. lines \"@jit(nopython=True)\"")
    print("    - replace `prange(quenches)` with `range(quenches)` around line 170")
    print("Install numba with 'pip install numba'")
    exit(1)
    
SKIP_HUGE_LATTICE_CALCULATIONS = False  # Enable this if you want to make calculations in few minutes

@jit(nopython=True)
def calc_energy_for_cell(s, idx, H, J):
    """
    :param s:       s matrix (M×N) containing all spins
    :param idx:     2d index of s (tuple)
    :return:        Energy of cell
    """
    E = 0.0
    for n in [[0,1], [0,-1], [-1,0], [1,0]]:
        neighbour_idx = ((idx[0]+n[0])%s.shape[0], (idx[1]+n[1])%s.shape[1])
        mask = (s[neighbour_idx] == s[idx])
        E += - J * (int(mask)*2-1)
    E += - H * s[idx]
    return E

@jit(nopython=True)
def do_random_flips(s, num_flips, H, J, T):
    """
    Tries to flip spin `num_flip` times in lattice s. Does work only for 2-spin 
    case.
    :param s:           s matrix (M×N) containing all spins
    :param num_flips:   Number of random flips to do
    """
    beta = 1/T  # Inverse temperature (omit boltsmann constant)
    for _ in range(num_flips):
        idx = (np.random.randint(s.shape[0]), np.random.randint(s.shape[1]))
        # Factor 2 to J because neighbour cells' energy change too
        E0 = calc_energy_for_cell(s, idx, H, 2*J)  # Energy in current state
        s[idx] *= -1  # Flip
        E1 = calc_energy_for_cell(s, idx, H, 2*J) # Energy in flipped state
        p0 = np.exp(- beta * E0)  # Proportional probability in current state 
        p1 = np.exp(- beta * E1)  # ... flipped state 
        if p1/(p0 + p1) < np.random.rand():
            s[idx] *= -1  # Flip

@jit(nopython=True)            
def do_random_flips_qpotts(s, num_flips, q_potts, H, J, T):
    """
    Tries to change spin `num_flip` times in lattice s. Does work for q-state 
    Potts model.
    :param s:           s matrix (M×N) containing all spins
    :param num_flips:   Number of random flips to do
    """
    beta = 1/T
    for _ in range(num_flips):
        idx = (np.random.randint(s.shape[0]), np.random.randint(s.shape[1]))
        states = np.arange(q_potts)*2 - q_potts//2
        ps = np.zeros(q_potts+1)  # cumulative probabilities
        for i in range(q_potts):
            s[idx] = states[i]
            E = calc_energy_for_cell(s, idx, H, 2*J)
            ps[i+1] = np.exp(- beta * E) + ps[i]
        rnd = ps[-1] * np.random.rand()  # ps[-1] = Z the partition function
        new_state = 9999999
        for i in range(q_potts):
            if rnd <= ps[i+1]:
                new_state = states[i]
                break
        assert(new_state < q_potts*2-q_potts//2)
        s[idx] = new_state
            
@jit(nopython=True)
def calc_total_energy_inefficient(s, H, J):
    E = 0.0
    for i in range(s.shape[0]):
        for j in range(s.shape[1]):
            # Calculate only every other cell because other `calc_energy_for_cell`
            # takes into account all neigbouring 4 sites of cell.
            if (i%2==0) ^ (j%2==0):  # the `^` symbol is XOR. This creates chessboard pattern
                E += calc_energy_for_cell(s, (i,j), H, J)
    return E

@jit(nopython=True)
def calc_total_energy(s, H, J):
    """
    Energy of whole lattice. Does not work for q-potts model.
    
    Nitty picky CPU optimization details:
    This function does the same as `calc_total_energy_inefficient`,
    but that is not very CPU cache friendly due to the disalignment in memory 
    layoyt. Benchmarks show that in pure python this optimized function is 300× 
    faster. If numba is used in both cases, it is still 20 faster than
    than `calc_total_energy_inefficient`.
    
    :param s:       s matrix (M×N) containing all spins
    :param H:       External magnetic field strength
    
    :return:        Energy of system
    """
    s_shift = np.empty_like(s)
    s_sum = np.empty_like(s) # Sum of shifts of s (up down left righ)
    # Upwards shift
    s_shift[:-1,:] = s[1:,:]
    s_shift[-1,:] = s[0,:]
    s_sum[:,:] = s_shift 
    # Downwars shift
    s_shift[1:,:] = s[:-1,:]
    s_shift[0,:] = s[-1,:]
    s_sum += s_shift 
    # Left shift
    s_shift[:,:-1] = s[:,1:]
    s_shift[:,-1] = s[:,0]
    s_sum += s_shift 
    # Right shift
    s_shift[:,1:] = s[:,:-1]
    s_shift[:,0] = s[:,-1]
    s_sum += s_shift 
    # Divided with 2 because this calculates energy coupling energy twice
    E_cells = -J/2 * s * s_sum - H * s
    return np.sum(E_cells)
            
@jit(nopython=True)
def quench(L, T, t_max, sample_rate=50, H=0.0, J=1.0, q_potts=2):
        """
        
        
        The simulation time is measured with average steps per spin. Therefore
        with 100×100 lattice, there's 10 000 Monte Carlo steps in every time 
        unit.
        
        :param L:           Lattice size
        :param T:           Temperature to which system is quenched
        :param t_max:       Total simulation time 
        :param sample_rate: Samples in one unit simulation time (default 5)
        
        :return:
                    Final configuration of ising model (size: [L×L])
                    Magnetization vector (size: [t_max * sample_rate])
                    Total enery of system (size: [t_max * sample_rate])
                    Corresponding time values vector
                    
        """
        # Numba is sometimes frustrating, it makes using numpy very clumpy:
        s = np.zeros((L,L), dtype=np.int_)
        for i in range(L):
            for j in range(L):
                s[i,j] = np.random.randint(q_potts)*2 - q_potts//2
        
        m_vec = np.full(t_max*sample_rate, fill_value=np.nan)  
        E_vec = np.full(t_max*sample_rate, fill_value=np.nan)  
        if q_potts==2:
            for t_idx in range(t_max*sample_rate):
                do_random_flips(s, int(L*L/sample_rate), H, J, T)
                m_vec[t_idx] = np.mean(s)
                E_vec[t_idx] = calc_total_energy(s, H, J)
        else:  # q-potts
            for t_idx in range(t_max*sample_rate):
                do_random_flips_qpotts(s, int(L*L/sample_rate), q_potts, H, J, T)
                m_vec[t_idx] = np.max(np.bincount(((s+q_potts//2)//2).flat)) / (L*L)
                E_vec[t_idx] = calc_total_energy_inefficient(s, H, J)
        return s, m_vec, E_vec
    
@jit(nopython=True, parallel=True)
def parallel_quenches(quenches, L, T, t_max, sample_rate, H, J, q_potts=2):
    sim_results = np.zeros((quenches, 2, t_max*sample_rate))
    for quench_idx in prange(quenches):  # repeat simulation
        # Avoid that different threads the same copy of intial state of rand-generator.
        #np.random.seed(quench_idx)  # Wow, is it fixed automatically?
        # Do the calculation
        s, m_vec, E_vec = quench(L, T, t_max, sample_rate, H=H, J=J, q_potts=q_potts)
        sim_results[quench_idx, 0, :] = m_vec
        sim_results[quench_idx, 1, :] = E_vec
    return sim_results
        
def simulate_domain_growth(T, L, plot=False, q_potts=2, t_max=128):
    """
    Calculates the domain growth exponent factors for chessboard model and circle 
    model. If q_potts!=2, only chessboard solution is valid. Also plots figures 
    on the way.
    
    Return
        alphas  [(⍺_c_mean, ⍺_c_err), (⍺_d_mean, ⍺_d_err)]
                where '⍺' is growth factor (type float), 'c' chessboard model 
                and 'd' circle model. Err is 2 sigma = 98%.
        Rs      [(R_c_mean, R_c_err), (R_d_mean, R_d_err)]
                where 'R' is vector of domain sizes, correspondin time values t_vec
        idxs    [idx_c, idx_d]
                where 'idx' is index after which R values can be considerd bad. 
                The coefficient alpha is calculated from range of values R[:idx].
        t_vec   Time values correponding R vector
    """
    H = 0.0             # Magnetic field, zero in this problem
    J = 1.0             # Coupling constant
    sample_rate = 5     # Calculate observables this many times in one unit time
    quenches = 30       # Repeat simulation this many times
    t_vec = np.linspace(0, t_max, t_max*sample_rate, endpoint=False) + 1.0
    
    observables = {# Magnetization
               "m_vec": ("$m$", lambda m, _, __: m), 
               # Energy per spin
               "ϵ_vec": ("$ϵ$", lambda _, __, ϵ: ϵ),  
               # My chessboard domain size estimate
               "c_vec": ("$c$", lambda _, __, ϵ: 4*J / (2*J + ϵ) ),
               # My circle domain size estimate (diameter, not radius)
               "d_vec": ("$d$", lambda _, m2, ϵ: 2*16 * J * m2 / (np.pi * (ϵ + 2*J - H*(1-2*m2))))}
    
    if plot:
        axes = {}
        for name, (ylabel, _) in observables.items():
            fig, ax = plt.subplots(1, 1, figsize=(6.4,4.8), num=name)
            if plot:
                ax.set_xlabel("$t$")
                ax.set_ylabel(ylabel)
                axes[name] = ax
        # Tune domain growth observables
        for name in ["d_vec", "c_vec"]:
            axes[name].set_xscale("log")
            axes[name].set_yscale("log")
            # Set larges domain size in figure to be length of grid
            axes[name].set_ylim(2, 1.5*L)
        # Draw markers when magnetization is too large for our domain growth meters
        axes["m_vec"].plot([1, t_max+1], [0.57, 0.57], c="black")
        axes["m_vec"].plot([1, t_max+1], [-0.57, -0.57], c="black")
    
    # Run simulation parallel
    sim_results = parallel_quenches(quenches, L, T, t_max, sample_rate, H, J, q_potts)
    
    # Read data and form observables (i.e. simulationg growth)
    obs_results = {name: np.zeros((quenches, t_max*sample_rate)) for name in observables.keys()}
    for quench_idx in range(quenches):    
        m_vec = sim_results[quench_idx, 0, :]
        E_vec = sim_results[quench_idx, 1, :]
        ϵ_vec = E_vec / (L*L)
        m2_vec = (np.abs(m_vec)+1) / 2  # Magnetization rescaled to range 0.5-1
                                        # We do not care which is way is "up"
        for name, (ylabel, f) in observables.items():
            y = f(m_vec, m2_vec, ϵ_vec)
            obs_results[name][quench_idx,:] = y
            if plot:
                # After this idx curve is out of bounds, and it is drawn with dashed line
                idxs = np.nonzero(np.abs(m_vec) > 0.57)[0]  # 0.57 = 1-(1-np.pi/4)*2
                if len(idxs) == 0:  # My circle apprixmation is valid (circle fits in square)
                    axes[name].plot(t_vec, y, c="C0", alpha=0.6)
                else:  # My circle apprixmation is invalid
                    axes[name].plot((t_vec)[:idxs[0]+1], y[:idxs[0]+1], c="C0", alpha=0.6)
                    axes[name].plot((t_vec)[idxs[0]:], y[idxs[0]:], 
                                    c="C0", alpha=0.5, ls=(0, (5, 5)))

    
    # Exponent coefficients and its errors. First item is chessboard model and second circular model 
    alphas = []
    # Domain size vectors and its errorvectors. First item is chessboard model and second circular model
    Rs = []
    idxs = []
    for obs_idx, name in enumerate(["c_vec", "d_vec"]):
        mask = np.abs(obs_results["m_vec"]) < 0.57  # valid values that are taken to mean
        counts = mask.sum(axis=0)
        # Mean (vectorized) cropping out invalid values
        R_mean = (obs_results[name] * mask).sum(axis=0) / counts
        # Standard deviation (vectorized) cropping out invalid values
        R_err = np.sqrt( ((obs_results[name]-R_mean)**2 * mask).sum(axis=0) / (counts-1))
        Rs.append((R_mean, 2 * R_err))  # 2 sigma error
        # Crop out values when 
        #     A) magnetization is out of bound for 50% of quenches 
        #     B) Domain size is (somewhy?!?) NaN or inf
        indices = np.nonzero(np.logical_or(counts < quenches//2, np.invert(np.isfinite(R_mean))))[0]
        if len(indices) == 0:
            idx = -1
        else:
            idx = max(indices[0], 2)
        idxs.append(idx)
        # Calculate exponent coefficient alpha
        t_log = np.log10(t_vec)
        R_mean_log = np.log10(R_mean)
        p = np.polyfit(t_log[:idx], R_mean_log[:idx], 1)
        # Calculate error for alpha
        alpha_quenches = np.zeros(quenches)
        for i in range(quenches):
            R_log = np.log10(obs_results[name][i,:])
            p2 = np.polyfit(t_log[:idx], R_log[:idx], 1)
            alpha_quenches[i] = p2[0]
        
        p_err = 2 * (alpha_quenches.std() / np.sqrt(quenches))  # 2 sigma error
        alphas.append((p[0], p_err))
        
        if plot:
            ylabel, _ = observables[name]
            ax = axes[name]
            
            y_fit_log = p[0]*t_log + p[1]
            ax.plot(10**t_log[:idx], 10**y_fit_log[:idx], color="C1", label="linear fit")
            ax.legend()
    return alphas, Rs, idxs, t_vec

            
def main():
    L0 = 40             # Grid length
    T0 = 0.5            # Temperature
    q0 = 2              # Q-state Potts
        
    print("part a)")
    (α_c0, α_d0), (R_c0, R_d0), idx0, t_vec = simulate_domain_growth(T0, L0, plot=True)
    print("T = {:.4f},  L = {},  --  α_c: ({:.4f}±{:.4f}),  α_d: ({:.4f}±{:.4f}), R_(t=10) = {:.4f}"
          .format(T0, L0, α_c0[0], α_c0[1], α_d0[0], α_d0[1], R_d0[0][50].mean()))
    
    print("part b and c)")
    samples = 11
    # Variate first T then L and last q.
    variations = ["T", "L", "q"]
    if SKIP_HUGE_LATTICE_CALCULATIONS:
        variations.pop(1)  # pop out ("L") variation from calculation
    for param_idx, sym in enumerate(variations):  
        # Plot domain size as a function of time for different parameters
        fig1, ax1 = plt.subplots(1, 1, figsize=(6.4,4.8), num="b-part domainsize c var "+sym)
        fig2, ax2 = plt.subplots(1, 1, figsize=(6.4,4.8), num="b-part domainsize d var "+sym)
        
        α_c_vec = np.zeros((2,samples))  # Chessboard model (×2 because errorbars)
        α_d_vec = np.zeros((2,samples))  # Cricle model
        vecs = []
        
        T_vec = [T0 for L in range(samples)]
        L_vec = [L0 for L in range(samples)]
        q_vec = [q0 for L in range(samples)]
        if sym == "T":
            T_vec = x = [T0/2] + [T0 * (i+1) for i in range(samples-1)]
        elif sym == "L":
            # Note: Lattice sizes are calculated from L=20 to L=400. Latter take eternity!
            L_vec = x = [L0//2] + [L0 * (i+1) for i in range(samples-1)]
        elif sym == "q":
            q_vec = x = [i for i in range(q0, q0 + samples)]
            
        print("Variating: {}".format(sym))
        # Two vectors from T, L, q are constant and one is varied
        for i, (T, L, q) in enumerate(zip(T_vec, L_vec, q_vec)):
            if T == T0 and L == L0 and q==q0:
                # Reuse the a)-part instead of recalculating
                α_c, α_d, R_c, R_d, idx_c, idx_d = α_c0, α_d0, R_c0, R_d0, idx0[0], idx0[1]
            else:
                (α_c, α_d), (R_c, R_d), (idx_c, idx_d), t_vec = simulate_domain_growth(T, L, plot=False, q_potts=q)
            α_c_vec[:,i] = α_c
            α_d_vec[:,i] = α_d
            R_at_time_1 = R_d[0][5*10].mean()  
            print("T = {:.4f},  L = {},  q = {}, --  α_c: ({:.4f}±{:.4f}),  α_d: ({:.4f}±{:.4f}), R_(t=10) = {:.4f}"
                .format(T, L, q, α_c[0], α_c[1], α_d[0], α_d[1], R_at_time_1))
            if i in [0,1] or i in [samples//2, samples-1]:
                ls = "--" if i==samples-1 else "-"
                ax1.plot(t_vec[:idx_c], R_c[0][:idx_c], ls=ls, c="C{}".format(i%6), label="${} = {}$".format(sym, x[i]))
                ax2.plot(t_vec[:idx_d], R_d[0][:idx_d], ls=ls, c="C{}".format(i%6), label="${} = {}$".format(sym, x[i]))
            if i==(samples-1):
                ax1.plot(t_vec[[0,idx_c]], R_c[0][[0,idx_c-1]], label="straight line", ls=(0, (5, 5)), color="black", lw=0.5)
                ax2.plot(t_vec[[0,idx_d]], R_d[0][[0,idx_d-1]], label="straight line", ls=(0, (5, 5)), color="black", lw=0.5)
        for ax, ylabel in [(ax1, "$c$"), (ax2, "$d$")]:
            ax.set_xlabel("$t$")
            ax.set_ylabel(ylabel)
            ax.legend()
            ax.set_xscale("log")
            ax.set_yscale("log")
        fig, ax = plt.subplots(1, 1, figsize=(6.4,4.8), num="b-part var "+sym)
        ax.errorbar(x, α_c_vec[0,:], α_c_vec[1,:], label="chessboard model", capsize=4)
        ax.errorbar(x, α_d_vec[0,:], α_d_vec[1,:], label="circle model", capsize=4)
        ax.set_xlabel("${}$".format(sym))
        ax.set_ylabel("α")
        fig.tight_layout()
        ax.legend()
        
    if not SKIP_HUGE_LATTICE_CALCULATIONS:
        fig, ax = plt.subplots(1, 1, figsize=(6.4,4.8), num="long_time")
        for i, T in enumerate([0.5, 2.5, 3.5, 4.5]):
            (α_c, α_d), (R_c, R_d), (idx_c, idx_d), t_vec = simulate_domain_growth(T=T, L=100, plot=False, q_potts=2, t_max=1028)
            print("One long simulation (t_max=1028), T={} α_c: ({:.4f}±{:.4f}),  α_d: ({:.4f}±{:.4f})"
                .format(T, α_c[0], α_c[1], α_d[0], α_d[1]))
            ax.plot(t_vec[:idx_c], R_c[0][:idx_c], c="C"+str(i), label="$T={}$".format(T))
            ax.fill_between(t_vec[:idx_c], R_c[0][:idx_c] + R_c[1][:idx_c], R_c[0][:idx_c] - R_c[1][:idx_c], color="C"+str(i), alpha=0.2)
        ax.set_xlabel("t")
        ax.set_ylabel("c")
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.legend()
            
if __name__ == "__main__":
    print("On my machine, running this script took about 2 hours. It takes so long because")
    print("huge lattices like L=400 are calculated. Enable `SKIP_HUGE_LATTICE_CALCULATIONS`")
    print("to reduce calculation time to something like 15 minutes.")
    start = time.time()
    main()
    end = time.time()
    print("Time elapsed in seconds: ", end - start)
    plt.show()

