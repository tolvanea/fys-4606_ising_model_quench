"""
This file draws example figures used in theory-section. The real results are calculated in `ising.py`.

If you want to play around with code, this file is better place to start, because it is simpler and faster.
"""
import numpy as np
import matplotlib.pyplot as plt

import ising

L0 = 120              # Grid length
T0 = 0.5             # Temperature
q = 2               # Q-state Potts
H = 0.0             # Magnetic field, zero in this problem
J = 1.0             # Coupling constant
t_max = 128         # Simulation time
sample_rate = 5     # Calculate observables this many times in one unit time


def draw_2d_time_evolution(q_potts=2, T=T0, cmap=None, L=L0):
    fig, axs = plt.subplots(1, 3, figsize=(10, 4), num="evolution_q{}_T{}_L{}".format(q_potts, T, L))
    
    s = np.random.randint(q_potts, size=(L,L))*2 - q_potts//2
    for i in range(3):
        t = L*L*(10**(i))
        ising.do_random_flips_qpotts(s, t, q_potts, H, J, T)
        ϵ = ising.calc_total_energy_inefficient(s, H, J) / (L*L)
        m = np.max(np.bincount(((s+q_potts//2)//2).flat)) / (L*L)
        m2 = (np.abs(m) + 1) / 2
        
        c = 4*J / (2*J + ϵ)
        d = 32 * J * m2 / (np.pi * (ϵ + 2*J - H*(1-2*m2)))
        #c=d
        
        axs[i].imshow(s, cmap=cmap, extent=(0, L, 0, L))
        axs[i].plot([L/2-c/2, L/2+c/2], [L*0.1, L*0.1], c="red", lw=2)
        for x in [L/2-c/2, L/2+c/2]:
            axs[i].plot([x,x], [L*0.085, L*0.115], c="red", lw=2)
        axs[i].text(L/2, L*0.15, "R = {:.2f}".format(c), 
                     fontsize=12, verticalAlignment="bottom", horizontalAlignment="center",
                     bbox=dict(boxstyle='round', facecolor='white', alpha=0.99))
        axs[i].set_title("t = {:.2f}".format(t/(L*L)))
        
    fig.tight_layout()
    
def draw_domain_assumptions():
    L = L0
    c = L//6
    X, Y = np.meshgrid(np.arange(L), np.arange(L), indexing="ij")
    
    s1 = np.full((L,L), fill_value=-1)
    up1 = np.logical_xor((X // c)%2==0, (Y // c)%2==0)
    s1[up1] = 1
    
    s2 = np.full((L,L), fill_value=-1)
    up2 = (X%c-c//2+0.5)**2 + (Y%c-c//2+0.5)**2 < (1/np.sqrt(2*np.pi)*c)**2
    s2[up2] = 1
    
    
    for s, name in [(s1, "square"), (s2, "circle")]:
        fig, ax = plt.subplots(1, 1, figsize=(6.4*0.8,4.8), num=name)
        ax.imshow(s, extent=(0, L, 0, L))
        
        crds = [[c, c], [c, 2*c], [2*c, 2*c], [2*c, c]]
        for i in range(4):
            ax.plot(crds[i], crds[(i+1)%4], color="red", lw=2)

def main():
    draw_2d_time_evolution()
    draw_2d_time_evolution(q_potts=4)
    draw_2d_time_evolution(T=3.5)
    draw_domain_assumptions()
    
if __name__ == "__main__":
    main()
    plt.show()
